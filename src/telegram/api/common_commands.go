package api

type SendMessage struct {
	ChatId                int64  `json:"chat_id"`
	Text                  string `json:"text"`
	ParseMode             string `json:"parse_mode,omitempty"`
	DisableWebPagePreview *bool  `json:"disable_web_page_preview"`
	DisableNotification   *bool  `json:"disable_notification"`
	ReplyToMessageId      *int64 `json:"reply_to_message_id"`
	//ReplyMarkup *
}

type SendMessageResponse struct {
	// whether request succeeded
	Ok bool
	// chat requested
	Result *Message
}

type DeleteMessage struct {
	ChatId    int64 `json:"chat_id"`
	MessageId int64 `json:"message_id"`
}
